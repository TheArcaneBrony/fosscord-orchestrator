using System.Diagnostics;
using FosscordOrchestrator;
using FosscordOrchestrator.DbModel;
using FosscordOrchestrator.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Npgsql;

Util.Log("Reading config");
RuntimeInfo.config = Config.Read();
RuntimeInfo.config.Save();
if(!Directory.Exists("logs")) Directory.CreateDirectory("logs");

Db db = RuntimeInfo.config.GetDbContext();
db.Database.Migrate();
int i = db.InstanceConfigs.Count();
while (i <= 50)
{
    Util.Log("Adding instance " + i);
    db.InstanceConfigs.Add(new()
    {
        InstanceName = "TheArcaneBrony"+i,
        InstanceDbName = "fosscord_tab"+i
    });
    try
    {
        db.Database.ExecuteSqlRaw($"CREATE DATABASE fosscord_tab{i};");
    }
    catch
    {
        Util.Log("Db exists");
    }
    i++;
}

db.SaveChanges();

Task.Run(() =>
{

    FosscordInstallation install = new FosscordInstallation();
    install.Setup();


    RuntimeInfo.Processes.AddRange(db.InstanceConfigs.Select(x => new FosscordInstance(x)));
    foreach (var inst in RuntimeInfo.Processes)
    {
        inst.Start();
    }

    Thread.Sleep(5000);
    while (true)
    {
        foreach (var inst in RuntimeInfo.Processes.Where(x=>x.process.HasExited))
        {
            inst.Start();
            Thread.Sleep(1000);
        }
        foreach (var inst in RuntimeInfo.Processes.Where(x=>!x.process.HasExited))
        {
            inst._cfg.Details.MemUsage = inst.MemUsage;
            inst._cfg.Details.ProcStatus = inst.process.HasExited ? "dead" : "alive";
            NpgsqlConnection conn = new NpgsqlConnection($"postgres://{RuntimeInfo.config.PostgresDb.Username}:{RuntimeInfo.config.PostgresDb.Password}@{RuntimeInfo.config.PostgresDb.DbAddress}/{inst._cfg.InstanceDbName}");
            conn.Open();
            inst._cfg.Details.UserCount = (int) new NpgsqlCommand("select count(*) from users", conn).ExecuteScalar();
            conn.Close();
        }
        Thread.Sleep(5000);
    }
});


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new() { Title = "FosscordOrchestrator", Version = "v1" }); });
builder.Services.AddDbContextPool<Db>(optionsBuilder =>
{
    optionsBuilder.UseNpgsql($"User ID={RuntimeInfo.config.PostgresDb.Username}; Password={RuntimeInfo.config.PostgresDb.Password}; Host={RuntimeInfo.config.PostgresDb.DbAddress}; Port={RuntimeInfo.config.PostgresDb.DbPort}; Database={RuntimeInfo.config.PostgresDb.DbName}");
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "FosscordOrchestrator v1"));
}

// app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();