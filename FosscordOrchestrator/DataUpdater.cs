using FosscordOrchestrator.DbModel;

namespace FosscordOrchestrator;

public class DataUpdater
{
    private static Db db = new Db();
    public static void UpdateData()
    {
        foreach (var proc in RuntimeInfo.Processes)
        {
            var dbi = db.InstanceConfigs.First(x => x.InstanceConfigId == proc._cfg.InstanceConfigId);
            dbi.Details.MemUsage = proc.MemUsage;
            
        }
    }
}