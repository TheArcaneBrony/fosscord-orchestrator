using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace FosscordOrchestrator
{
    public class Util
    {
        public static void Log(string message,
            bool LogAlways = true,
            [CallerFilePath] string file = null,
            [CallerLineNumber] int line = 0)          
        {
            if(Debugger.IsAttached) Console.WriteLine("{0}:{1} {2}", Path.GetFileName(file), line, message);
            else if(LogAlways) Console.WriteLine(message);
        }
        public static void LogDebug(string message,
            bool LogAlways = false,
            [CallerFilePath] string file = null,
            [CallerLineNumber] int line = 0)          
        {
            if(Debugger.IsAttached) Debug.WriteLine("{0}:{1} {2}", Path.GetFileName(file), line, message);
            else if(LogAlways) Debug.WriteLine(message);
        }
        
        public static Dictionary<string, int> GetGitCommitCounts()
        {
            Dictionary<string, int> commitCounts = new();
            ProcessStartInfo psi = new ProcessStartInfo("git", "shortlog -s -n --all")
            {
                CreateNoWindow = true,
                RedirectStandardOutput = true
            };
            Process proc = Process.Start(psi);
            while (!proc.StandardOutput.EndOfStream)
            {
                string line = proc.StandardOutput.ReadLine() ?? "";
                Util.Log(line);
                if (line.Length > 4)
                {
                    string[] commits = line.Trim().Split("\t");
                    if (commits[1] == "TheArcaneBrony") commits[1] = "The Arcane Brony";
                    if (commits[1] == "Christopher Cookman") commits[1] = "Chris Chrome";
                    if (commitCounts.ContainsKey(commits[1] ?? "Unknown"))
                    {
                        commitCounts[commits?[1] ?? "Unknown"] += int.Parse(commits?[0] ?? "-1");
                    }
                    else commitCounts.Add(commits?[1] ?? "Unknown", int.Parse(commits?[0]??"-1"));
                }
            }
            return commitCounts;
        }

        public static void RunCommandSync(string command, string args = "", bool silent = false)
        {
            RunCommandInDirSync(Environment.CurrentDirectory, command, args, silent);
        }
        public static void RunCommandInDirSync(string path, string command, string args = "", bool silent = false)
        {
            Util.Log($"Running `{command} {args}` in {path}");
            Process.Start(new ProcessStartInfo()
            {
                WorkingDirectory = path,
                FileName = command,
                Arguments = args,
                RedirectStandardOutput = !silent
            })?.WaitForExit();
        }

        public static string GetCommandOutputSync(string command, string args = "", bool silent = true)
        {
            return GetCommandOutputInDirSync(Environment.CurrentDirectory, command, args, silent);
        }

        public static string GetCommandOutputInDirSync(string path, string command, string args = "", bool silent = true)
        {
            ProcessStartInfo psi = new ProcessStartInfo(command, args)
            {
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                WorkingDirectory = path
            };
            Process proc = Process.Start(psi);
            string output = "";
            while (!proc.StandardOutput.EndOfStream)
            {
                string line = proc.StandardOutput.ReadLine() ?? "";
                Util.Log(line);
                output += line + "\n";
            }
            return output;
        }
    }
}