using System.IO;
using FosscordOrchestrator.DbModel;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace FosscordOrchestrator;

public class Config
{
    public DbAuth PostgresDb { get; set; } = new();
    public string FosscordDir { get; set; } = "/root/fosscord";
    public Db GetDbContext()
    {
        return new Db();
    }
    public static Config Read()
    {
        return (File.Exists("config.json")
            ? JsonConvert.DeserializeObject<Config>(File.ReadAllText("config.json"))
            : new Config()) ?? new Config();
    }

    public void Save()
    {
        File.WriteAllText("config.json", JsonConvert.SerializeObject(this));
    }
    
}
public class DbAuth
{
    public string Username = "postgres";
    public string Password = "postgres";
    public string DbAddress = "localhost";
    public int DbPort = 5432;
    public string DbName { get; set; } = "FosscordOrchestrator";
}