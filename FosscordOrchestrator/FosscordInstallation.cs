using System.Diagnostics;

namespace FosscordOrchestrator;

public class FosscordInstallation
{
    public void Setup()
    {
        if (Directory.Exists(RuntimeInfo.config.FosscordDir))
        {
            // Directory.Delete(RuntimeInfo.config.FosscordDir, true);
            Util.Log("Checking for updates...");
            Util.RunCommandInDirSync(RuntimeInfo.config.FosscordDir, "git", "fetch");
            if (Util.GetCommandOutputInDirSync(RuntimeInfo.config.FosscordDir, "git", "status", false)
                .Contains("Your branch is up to date"))
            {
                Util.Log("Up to date");
            }
            else
            {
                Util.Log("Not up to date, pulling changes!");
                Util.RunCommandInDirSync(RuntimeInfo.config.FosscordDir, "git", "reset --hard origin/HEAD");
                Util.RunCommandInDirSync(RuntimeInfo.config.FosscordDir+"/bundle", "npm", "run build clean");
            }
        }
        else
        {
            Util.Log("Downloading fosscord...");
            Process.Start("git", "clone https://github.com/fosscord/fosscord-server " + RuntimeInfo.config.FosscordDir).WaitForExit();
            Util.Log("Setting up fosscord...");
            Util.RunCommandInDirSync(RuntimeInfo.config.FosscordDir+"/bundle", "npm", "run setup", false);   
        }
    }
}