using System.Diagnostics;
using FosscordOrchestrator.Entities;
using Newtonsoft.Json;

namespace FosscordOrchestrator;

public class RuntimeInfo
{
    public static Config config;
    public static List<FosscordInstance> Processes = new();
    public static int Port = 3000;
}