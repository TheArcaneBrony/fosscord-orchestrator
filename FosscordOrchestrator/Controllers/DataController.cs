using FosscordOrchestrator.DbModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FosscordOrchestrator.Controllers;

[ApiController]
[Route("")]
public class DataController : ControllerBase
{
    
    private readonly ILogger<DataController> _logger;
    private Db _db;
    
    public DataController(ILogger<DataController> logger, Db db)
    {
        _logger = logger;
        _db = db;
    }

    [HttpGet("instances")]
    public ContentResult Get()
    {
        string resp = "<meta http-equiv='refresh' content='1'><table border=1>" + GetTableRow("ID,Name,Status,Mem,Users".Split(","));
        foreach (var inst in RuntimeInfo.Processes)
        {
            resp += GetTableRow(inst._cfg.InstanceConfigId, inst._cfg.InstanceName,
                inst.process.HasExited ? "Dead" : "Running", $"{inst.MemUsage / 1024d / 1024d:F2} MB",
                inst._cfg.Details.UserCount);
        }

        resp += "\n</table>";
        return new ContentResult()
        {
            Content = resp,
            ContentType = "text/html"
        };
    }

    private string GetTableRow(params object[] args)
    {
        return $"<tr>{string.Join("", args.Select(x=>$"<td>{x}</td>"))}</tr>";
    }
}