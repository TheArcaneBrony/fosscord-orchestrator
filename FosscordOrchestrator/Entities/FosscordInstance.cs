using System.Diagnostics;
using FosscordOrchestrator.DbModel;

namespace FosscordOrchestrator.Entities;

public class FosscordInstance
{
    public InstanceConfig _cfg;
    public Process process;
    public long MemUsage => process.PrivateMemorySize64;
    public long PID => process.Id;

    public FosscordInstance(InstanceConfig cfg)
    {
        _cfg = cfg;
    }
    public void Start()
    {
        Console.WriteLine($"Starting instance {_cfg.InstanceName}");
        var gcfg = RuntimeInfo.config;
        var psi = new ProcessStartInfo()
        {
            WorkingDirectory = "/root/fosscord-server/bundle",
            FileName = "npm",
            Arguments = $"run start:bundle",
            RedirectStandardOutput = true,
            RedirectStandardError = true
        };
        
        Console.WriteLine($"Running {psi.FileName} {psi.Arguments}");
        psi.Environment.Add("DATABASE",$"postgres://{gcfg.PostgresDb.Username}:{gcfg.PostgresDb.Password}@{gcfg.PostgresDb.DbAddress}/{_cfg.InstanceDbName}");
        psi.Environment.Add("PORT",""+RuntimeInfo.Port++);
        process = Process.Start(psi);
        bool print = true;
        StreamWriter log = File.AppendText($"{Environment.CurrentDirectory}/logs/{_cfg.InstanceName}.log");
        Task.Run(() =>
        {
            while (!process.HasExited)
            {
                string line = process.StandardOutput.ReadLine() ?? "";
                if (line.Length >= 2)
                {
                    if(print) Console.WriteLine($"[{_cfg.InstanceName}/STDOUT] {line}");
                    log.WriteLine($"[STDOUT] {line}");    
                }
            }
        });
        Task.Run(() =>
        {
            while (!process.HasExited)
            {
                string line = process.StandardError.ReadLine() ?? "";
                if(line.Length >= 2){
                    if(print) Console.WriteLine($"[{_cfg.InstanceName}/STDERR] {line}");
                    log.WriteLine($"[STDERR] {line}");
                }
            }
        });
    }
}