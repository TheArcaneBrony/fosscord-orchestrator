using Microsoft.EntityFrameworkCore;

namespace FosscordOrchestrator.DbModel;

public class Db : DbContext
{
    public DbSet<InstanceConfig> InstanceConfigs { get; set; } 
    public DbSet<InstanceDetails> InstanceDetails { get; set; }

    public Db(DbContextOptions? optionsBuilder = null)
    {
        OnConfiguring(new DbContextOptionsBuilder(optionsBuilder ?? new DbContextOptionsBuilder().Options));
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql($"User ID={RuntimeInfo.config.PostgresDb.Username}; Password={RuntimeInfo.config.PostgresDb.Password}; Host={RuntimeInfo.config.PostgresDb.DbAddress}; Port={RuntimeInfo.config.PostgresDb.DbPort}; Database={RuntimeInfo.config.PostgresDb.DbName}");
    }
}