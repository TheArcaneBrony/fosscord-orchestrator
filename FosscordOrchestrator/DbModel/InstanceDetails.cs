namespace FosscordOrchestrator.DbModel;

public class InstanceDetails
{
    public int InstanceDetailsId { get; set; }
    public int UserCount { get; set; } = 0;
    public long MemUsage { get; set; } = 0;
    public string ProcStatus { get; set; } = "Stopped";
}