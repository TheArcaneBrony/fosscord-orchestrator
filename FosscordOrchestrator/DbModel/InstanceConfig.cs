using System.Diagnostics;

namespace FosscordOrchestrator.DbModel;

public class InstanceConfig
{
    public int InstanceConfigId { get; set; }
    public string InstanceName { get; set; }
    public string InstanceDbName { get; set; }
    public InstanceDetails Details { get; set; } = new();
}