﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace FosscordOrchestrator.DbMigrations
{
    public partial class Migration0 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InstanceDetails",
                columns: table => new
                {
                    InstanceDetailsId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserCount = table.Column<int>(type: "integer", nullable: false),
                    MemUsage = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstanceDetails", x => x.InstanceDetailsId);
                });

            migrationBuilder.CreateTable(
                name: "InstanceConfigs",
                columns: table => new
                {
                    InstanceConfigId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    InstanceName = table.Column<string>(type: "text", nullable: false),
                    InstanceDbName = table.Column<string>(type: "text", nullable: false),
                    DetailsInstanceDetailsId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstanceConfigs", x => x.InstanceConfigId);
                    table.ForeignKey(
                        name: "FK_InstanceConfigs_InstanceDetails_DetailsInstanceDetailsId",
                        column: x => x.DetailsInstanceDetailsId,
                        principalTable: "InstanceDetails",
                        principalColumn: "InstanceDetailsId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InstanceConfigs_DetailsInstanceDetailsId",
                table: "InstanceConfigs",
                column: "DetailsInstanceDetailsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InstanceConfigs");

            migrationBuilder.DropTable(
                name: "InstanceDetails");
        }
    }
}
