﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FosscordOrchestrator.DbMigrations
{
    public partial class Migration1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "MemUsage",
                table: "InstanceDetails",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<string>(
                name: "ProcStatus",
                table: "InstanceDetails",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProcStatus",
                table: "InstanceDetails");

            migrationBuilder.AlterColumn<int>(
                name: "MemUsage",
                table: "InstanceDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint");
        }
    }
}
